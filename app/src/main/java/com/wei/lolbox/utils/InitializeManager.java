package com.wei.lolbox.utils;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.facebook.stetho.Stetho;
import com.github.moduth.blockcanary.BlockCanary;
import com.github.moduth.blockcanary.BlockCanaryContext;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;
import com.squareup.leakcanary.LeakCanary;
import com.tencent.bugly.crashreport.CrashReport;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * 作者：赵若位
 * 时间：2018/11/20 22:50
 * 邮箱：1070138445@qq.com
 * 功能：
 */
public class InitializeManager extends IntentService
{
    private static final String TAG = InitializeManager.class.getSimpleName();
    private static final String BUGLY_ID = "5102bec908";

    public InitializeManager()
    {
        super(TAG);
    }

    public static void init(Context context)
    {
        Intent it = new Intent(context, InitializeManager.class);
        it.setAction(TAG);
        context.startService(it);
    }

    @Override
    protected void onHandleIntent(Intent intent)
    {
        //耗时操作
        if (intent != null)
        {
            String tag = intent.getAction();
            if (!TextUtils.isEmpty(tag) && tag.equals(TAG))
            {
                initialize();
            }
        }
    }

    private void initialize()
    {
        //Log打印日志
        Logger.addLogAdapter(new AndroidLogAdapter());
        //数据库调试
        Stetho.initializeWithDefaults(this);
        //内存泄漏
        LeakCanary.install(getApplication());
        //UI过度绘制，卡顿
        BlockCanary.install(this, new BlockCanaryContext()).start();
        //Bugly
        initBugly();
    }

    /*初始化Bugly*/
    private void initBugly()
    {
        Context context = getApplicationContext();
        // 获取当前包名
        String packageName = context.getPackageName();
        // 获取当前进程名
        String processName = getProcessName(android.os.Process.myPid());
        // 设置是否为上报进程
        CrashReport.UserStrategy strategy = new CrashReport.UserStrategy(context);
        strategy.setUploadProcess(processName == null || processName.equals(packageName));
        // 初始化Bugly
        CrashReport.initCrashReport(context, BUGLY_ID, false, strategy);
    }


    /*获取进程号对应的进程名*/
    private String getProcessName(int pid)
    {
        BufferedReader reader = null;
        try
        {
            reader = new BufferedReader(new FileReader("/proc/" + pid + "/cmdline"));
            String processName = reader.readLine();
            if (!TextUtils.isEmpty(processName))
            {
                processName = processName.trim();
            }
            return processName;
        } catch (Throwable throwable)
        {
            throwable.printStackTrace();
        } finally
        {
            try
            {
                if (reader != null)
                {
                    reader.close();
                }
            } catch (IOException exception)
            {
                exception.printStackTrace();
            }
        }
        return null;
    }
}
